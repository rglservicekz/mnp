<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\SyncRunApp',
		'App\Console\Commands\SyncPortAll',
		'App\Console\Commands\SyncPortAllFull',
		'App\Console\Commands\SyncPortIncrement',
		'App\Console\Commands\SyncReturnIncrement',
		'App\Console\Commands\DownloadFiles',
		'App\Console\Commands\ReDownloadFiles',
		'App\Console\Commands\SyncPortIncrementLate',
		'App\Console\Commands\SyncPortAllFullLate',

	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		//$schedule->command('inspire')
		//		 ->hourly();
	}

}
