<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;

class SyncRunApp extends Command {

	protected $name = 'sync-run-app';

	protected $description = 'Run MNP application (all operations).';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$start_time = date("Hi");

		Log::info('---------- [ Start MNP app ' . date("Y-m-d H:i:s") . ' ] ----------');

		// run download  operation 
		$this->call('sync-download-files');

		// usleep before parsing files
		sleep(1);

		// run download  operation 
		$this->call('sync-re-download-files');

		// usleep before parsing files
		sleep(1);

		// run Port All Full (once at 00:15) 
		$this->call('sync-port-all-full');

		// usleep before Port Increment
		sleep(1);

		// run Port Increment 
		$this->call('sync-port-increment');

		// usleep before Return Increment
		sleep(1);
		// run Return Increment 
		// $this->call('sync-return-increment'); 

		// run Port All Full Late
		$this->call('sync-port-all-full-late');

		// usleep before Return Increment
		sleep(1);

		// run Port Increment Late
		$this->call('sync-port-increment-late');

		// usleep before Return Increment
		sleep(1);

		Log::info('----------- [ End MNP app ' . date("Y-m-d H:i:s") . ' ] -----------');
	}

}
