<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;

class SyncPortAllFullLate extends Command {

	protected $name = 'sync-port-all-full-late';

	protected $description = 'Sync Port All Full Late files.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$config = Config::get('mnp');
		$operation_type = 'port_all_full';

		Log::info('AFL > Start parsing Port All Full CSV ' . date("Y-m-d H:i:s"));

		$af_files = glob($config['local_folder_recent_late'] . 'Port_All_Full_*.csv');

		foreach ($af_files as $af_filename) {

			$handle = fopen($af_filename, "r");
			$row = 0;

			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				if ($row == 0) { $row++; continue; }
				if ($row == 1 && $data[5] == 0) 
				{ 
					Log::info('AFL > CSV file: ' . $af_filename . ' is empty! RowCount = ' . $data[5]);
					Log::info('AFL > Stopping proccess ...');
					break;
				}

				// check dup item
				$line_hash = md5($data[0] . $data[1] . $data[2] . $data[3] . $data[4] . $data[5]);
				$check_dup = PortedNumber::where('hash', 'LIKE', $line_hash)->count();
				if ($check_dup > 0)
				{
					Log::info('AFL > Phone number [' . $data[0] . '] already in DB [hash: ' . $line_hash . '].');
					Log::info('AFL > Skip.');

					$row++;
					continue;
				}

				$item = new PortedNumber;
		        $item->number 				= $data[0];
				$item->owner_id 			= $data[1];
				$item->mnc 					= $data[2];
				if (!empty($data[3])) {	$item->route = $data[3]; }
				$item->port_date 			= $data[4];
				if (!empty($data[5])) {	$item->row_count = $data[5]; }
				$item->inserted_at 			= date("Y-m-d H:i:S");
				$item->operation_type 		= $operation_type;
				$item->hash 				= $line_hash;
				$item->save();

				Log::info('AFL > Added new Number! [' . $data[0] . ' > ' . $data[1] . ' > ' . $data[4] . '].');

		        $row++;
		    }

			fclose($handle);

		}

		Log::info('AFL > End parsing Port All Full CSV ' . date("Y-m-d H:i:s"));
		Log::info('-----------------------------------------------------------');

	}
}
