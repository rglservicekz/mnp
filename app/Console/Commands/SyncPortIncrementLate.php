<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;

class SyncPortIncrementLate extends Command {

	protected $name = 'sync-port-increment-late';

	protected $description = 'Sync Port Increment files (re-downloaded).';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$config = Config::get('mnp');
		$operation_type = 'port_increment';

		Log::info('PIL > Start parsing Port Increment CSV ' . date("Y-m-d H:i:s"));

		$pi_files = glob($config['local_folder_recent_late'] . 'Port_Increment_*.csv');

		foreach ($pi_files as $pi_filename) {

			$handle = fopen($pi_filename, "r");
			$row = 0;

			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				if ($row == 0) { $row++; continue; }
				if ($row == 1 && $data[8] == 0) 
				{ 
					Log::info('PIL > CSV file: ' . $pi_filename . ' is empty! RowCount = ' . $data[8]);
					Log::info('PIL > Stopping proccess ...');
					break;
				}

				$line_hash = md5($data[0] . $data[1] . $data[2] . $data[3] . $data[4] . $data[5] . $data[6] . $data[7] . $data[8]);
				$check_dup = PortedNumber::where('hash', 'LIKE', $line_hash)->count();
				if ($check_dup > 0)
				{
					Log::info('PIL > Phone number [' . $data[1] . '] already in DB [hash: ' . $line_hash . '].');
					Log::info('PIL > Skip (Duplicate).');

					$row++;
					continue;
				}

				$item = new PortedNumber;
				$item->np_id 			= $data[0];
				$item->number 			= $data[1]; 
				$item->donor_id 		= $data[3]; 
				$item->recipient_id 	= $data[2]; 
				$item->rangeholder_id 	= $data[4];
				$item->route 			= $data[6];
				$item->old_route 		= $data[5];
				$item->port_date 		= $data[7];
				if (!empty($data[8])) {	$item->row_count = $data[8]; }
				$item->inserted_at 		= date("Y-m-d H:i:S");
				$item->operation_type 	= $operation_type;
				$item->hash 			= $line_hash;
				$item->save();

				Log::info('PIL > Added new Number! [' . $data[1] . ' > ' . $data[3] . ' > ' . $item->recipient_id . '].');

		        $row++;
		    }

			fclose($handle);

		}

		// clear Recent folder
		Log::info('PIL > Clearing recent_late folder ...');
		array_map('unlink', glob("/var/www/siesta2/mnp/storage/mnp_archives/recent_late/*"));
		Log::info('PIL > Done!');

		Log::info('PIL > End parsing Port Increment CSV ' . date("Y-m-d H:i:s"));
		Log::info('-----------------------------------------------------------');
	}

}
