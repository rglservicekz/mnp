<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;
use App\Models\Tele2Sync;
use App\Models\Dllog;

class ReDownloadFiles extends Command {

	protected $name = 'sync-re-download-files';

	protected $description = 'Re-download files/archives from MNP server.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{

		$config = Config::get('mnp');
		$dl_time = date("Y-m-d H:i:s"); 

		Log::info('RD > Start to re-download files ' . date("Y-m-d H:i:s"));

		// clear Recent folder
		Log::info('RD > Clearing recent_late folder ...');
		array_map('unlink', glob("/var/www/siesta2/mnp/storage/mnp_archives/recent_late/*"));
		Log::info('RD > Done!');

		// Get operator
		$operator = Operator::orderBy('priority', 'DESC')->first();


		///////////////////////////////////////// Kcell ///////////////////////////////////////

		if (strtolower($operator->name) == 'kcell')
		{
			Log::info('RD > ' . $operator->name . ' > Connecting to  ' . $operator->host . ' : ' . $operator->port . ' ...');
			// Connect to Kcell SFTP Server
			$sftp = KcellSync::connect($operator);
			if ($sftp == false) {
				Log::info('RD > ' . $operator->name . ' > Connection error!');
			} else {
				Log::info('RD > ' . $operator->name . ' > Connected ...');

				///////////////////////// RD Port All Full ////////////////////////

				//$rd_paf_items = Dllog::where('type', 2)->where('status', 0)->get();
				$rd_paf_items = Dllog::where('type', 2)->where('status', 0)->get();
				$a = 0;

				foreach ($rd_paf_items as $paf_item)
				{
					$a++;

					Log::info('RD > ' . $operator->name . ' > Re-downloading Port_All_Full archive (' . $a . '. mask: ' . $paf_item->filename_mask . ') ...');

					$af_result = KcellSync::re_download_port_all_full($sftp, $paf_item->filename_mask);
					if ($af_result == 'ok') {
						Log::info('RD > ' . $operator->name . ' > Done!');

						$dleditf = Dllog::find($paf_item->id);
						$dleditf->status = '1';
						$dleditf->dl_count += 1;
						$dleditf->save();
					} else {
						Log::info('RD > ' . $operator->name . ' > Error! Check connection or filename. It seems there is no file with given name.'); 

						$dleditf = Dllog::find($paf_item->id);
						$dleditf->status = '0';
						$dleditf->dl_count += 1;
						$dleditf->save(); 
					}

					if ($a == 40) break;

				}

				////////////////////////// RD Port Increment ////////////////////////
				
				//$rd_pi_items = Dllog::where('type', 1)->where('status', 0)->get();
				$rd_pi_items = Dllog::where('type', '1')->where('status', '0')->get();
				$p = 0;

				foreach ($rd_pi_items as $pi_item)
				{
					$p++;
				
					Log::info('RD > ' . $operator->name . ' > Re-downloading Port_Increment archive (' . $p . '. mask: ' . $pi_item->filename_mask . ') ...');

					$pi_result = KcellSync::re_download_port_increment($sftp, $pi_item->filename_mask);
					if ($pi_result == 'ok') {
						Log::info('RD > ' . $operator->name . ' > Done!');

						$dledit = Dllog::find($pi_item->id);
						$dledit->status = '1';
						$dledit->dl_count += 1; 
						$dledit->save();
					} else {
						Log::info('RD > ' . $operator->name . ' > Error! Check connection or filename. It seems there is no file with given name.'); 

						$dledit = Dllog::find($pi_item->id);
						$dledit->dl_count += 1; 
						$dledit->save();  
					}

					if ($p == 40) break;   
				}

				////////////////////////////////////////////////////////////////
				
				Log::info('RD > ' . $operator->name . ' > End re-download files'); 
				Log::info('-----------------------------------------------------------');

			}
		}
	}

}
