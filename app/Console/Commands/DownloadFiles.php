<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;
use App\Models\Tele2Sync;
use App\Models\Dllog;

class DownloadFiles extends Command {
	
	protected $name = 'sync-download-files';

	protected $description = 'Download files/archives from MNP server.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{

		$config = Config::get('mnp');
		$dl_time = date("Y-m-d H:i:s"); 
		$dl_time_ut = strtotime($dl_time);
		$dl_time_full = $dl_time_ut - 86400;

		Log::info('D > Start to download files ' . date("Y-m-d H:i:s"));

		// clear Recent folder
		Log::info('D > Clearing recent folder ...');
		array_map('unlink', glob("/var/www/siesta2/mnp/storage/mnp_archives/recent/*"));
		Log::info('D > Done!');

		// Get operator by priority
		//$operator = Operator::orderBy('id', 'ASC')->first();
		$operator = Operator::orderBy('priority', 'DESC')->first();

		if (date("i") == 15) {
			$pi_filename_mask = 'Port_Increment_' . date("YmdH") . '00';
		} else {
			$pi_filename_mask = 'Port_Increment_' . date("YmdH") . '30'; 
		}

		$af_filename_mask = 'Port_All_Full_' . date("Ym") . date("d", $dl_time_full) . '0000';

		///////////////////////////////////////// Kcell ///////////////////////////////////////

		if (strtolower($operator->name) == 'kcell')
		{

			if (date("Hi") == "0015")
			{
				// Add item to Download Log
				$dladdf = new Dllog;
				$dladdf->operator_id 	= $operator->id;
				$dladdf->dl_time 		= $dl_time;
				$dladdf->status 		= '0';
				$dladdf->type 			= '2';
				$dladdf->filename_mask	= $af_filename_mask;
				$dladdf->save();
			}

				// Add item to Download Log
				$dladdi = new Dllog;
				$dladdi->operator_id 	= $operator->id;
				$dladdi->dl_time 		= $dl_time;
				$dladdi->status 		= '0';
				$dladdi->type 			= '1';
				$dladdi->filename_mask	= $pi_filename_mask;
				$dladdi->save();

			Log::info('D > ' . $operator->name . ' > Connecting to  ' . $operator->host . ' : ' . $operator->port . ' ...');

			// Connect to Kcell SFTP Server
			$sftp = KcellSync::connect($operator);
			if ($sftp == false) {
				Log::info('D > ' . $operator->name . ' > Connection error!');
			} else {
				Log::info('D > ' . $operator->name . ' > Connected ...');

				///////////////////////// Port All Full ///////////////////////////

				if (date("Hi") == "0015")
				{
					// // Add item to Download Log
					// $dladdf = new Dllog;
					// $dladdf->operator_id 	= $operator->id;
					// $dladdf->dl_time 		= $dl_time;
					// $dladdf->status 		= '0';
					// $dladdf->type 			= '2';
					// $dladdf->filename_mask	= $af_filename_mask;
					// $dladdf->save();

					Log::info('D > ' . $operator->name . ' > Downloading Port_All_Full archive ...');
					// Download last Port_All_Full arch
					$af_result = KcellSync::download_port_all_full($sftp);
					if ($af_result == 'ok') {
						Log::info('D > ' . $operator->name . ' > Done!');
						// Change status
						$dleditf = Dllog::find($dladdf->id); 
						$dleditf->status = '1';
						$dleditf->dl_count += 1;
						$dleditf->save();
					} else {
						Log::info('D > ' . $operator->name . ' > Error! Check connection or filename. It seems there is no file with given name.'); 
						// Change status
						$dleditf = Dllog::find($dladdf->id);
						$dleditf->status = '0';
						$dleditf->dl_count += 1;
						$dleditf->save(); 
					}
				}

				////////////////////////// Port Increment ///////////////////////////

				// // Add item to Download Log
				// $dladdi = new Dllog;
				// $dladdi->operator_id 	= $operator->id;
				// $dladdi->dl_time 		= $dl_time;
				// $dladdi->status 		= '0';
				// $dladdi->type 			= '1';
				// $dladdi->filename_mask	= $pi_filename_mask;
				// $dladdi->save();
				
				Log::info('D > ' . $operator->name . ' > Downloading Port_Increment archive ...');
				// Download last Port_Increment arch
				$pi_result = KcellSync::download_port_increment($sftp);
				if ($pi_result == 'ok') {
					Log::info('D > ' . $operator->name . ' > Done!');
					// Change status
					$dledit = Dllog::find($dladdi->id);
					$dledit->status = '1';
					$dledit->dl_count += 1;
					$dledit->save();
				} else {
					Log::info('D > ' . $operator->name . ' > Error! Check connection or filename. It seems there is no file with given name.'); 
					// Change status
					$dledit = Dllog::find($dladdi->id);
					$dledit->dl_count += 1;
					$dledit->save(); 
				}

				////////////////////////////////////////////////////////////////

				Log::info('D > ' . $operator->name . ' > End download files');

			}

			Log::info('-----------------------------------------------------------');
		}
		
		/////////////////////////////////////////

		//////////////// Tele2 //////////////////
		/*
		if (strtolower($operator->name) == 'tele2')
		{
			Log::info('D > ' . $operator->name . ' > Connecting to  ' . $operator->host . ' : ' . $operator->port . ' ...');
			// Connect to Tele2 FTP Server
			$ftp = Tele2Sync::connect($operator);
			if ($ftp == false) {
				Log::info('D > ' . $operator->name . ' > Connection error!');
			} else {
				Log::info('D > ' . $operator->name . ' > Connected ...');
				if (date("Hi") == "0015")
				{

					// Add item to Download Log
					$dladdf = new Dllog;
					$dladdf->operator_id 	= $operator->id;
					$dladdf->dl_time	= $dl_time;
					$dladdf->status 		= '0';
					$dladdf->type 			= '2';
					$dladdf->save();

					Log::info('D > ' . $operator->name . ' > Downloading Port_All_Full archive ...');
					// Download last Port_All_Full arch
					Tele2Sync::download_port_all_full($ftp);
					Log::info('D > ' . $operator->name . ' > Done!');
					// Change status
					$dleditf = Dllog::find($dladdf->id);
					$dleditf->status = '1';
					$dleditf->save();
				}
				
				Log::info('D > ' . $operator->name . ' > Downloading Port_Increment archive ...');
				// Download last Port_Increment arch
				Tele2Sync::download_port_increment($ftp);
				Log::info('D > ' . $operator->name . ' > Done!');

				Log::info('D > ' . $operator->name . ' > Downloading Return_Increment archive ...');
				// Download last Return_Increment arch
				Tele2Sync::download_return_increment($ftp);
				Log::info('D > ' . $operator->name . ' > Done!');

				Log::info('D > ' . $operator->name . ' > End download files');
				Log::info('-----------------------------------------------------------');

				// Change status
				$dledit = Dllog::find($dladd->id);
				$dledit->status = '1';
				$dledit->save();
			}
		}
		*/
		/////////////////////////////////////////

	

	}


}
