<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Config;
use Log;

use App\Models\PortedNumber;
use App\Models\Operator;
use App\Models\Common;
use App\Models\KcellSync;

class SyncReturnIncrement extends Command {

	protected $name = 'sync-return-increment';

	protected $description = 'Sync Return Increment files.';

	public function __construct()
	{
		parent::__construct();
	}

	public function fire()
	{
		$config = Config::get('mnp');
		$operation_type = 'return_increment';

		Log::info('RI > Start parsing Return Increment CSV ' . date("Y-m-d H:i:s"));

		$ri_filename = glob($config['local_folder_recent'] . 'Return_Increment_*.csv');

		$handle = fopen($ri_filename[0], "r");
		$row = 0;

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
		{
			if ($row == 0) { $row++; continue; }
			if ($row == 1 && $data[7] == 0) 
			{ 
				Log::info('RI > CSV file: ' . $ri_filename[0] . ' is empty! RowCount = ' . $data[7]);
				Log::info('RI > Stopping proccess ...');
				break;
			}

			$line_hash = md5($data[0] . $data[1] . $data[2] . $data[3] . $data[4] . $data[5] . $data[6] . $data[7]);

			$item = new PortedNumber;
			$item->np_id 			= $data[0];
			$item->number 			= $data[1];
			$item->owner_id 		= $data[2];
			$item->rangeholder_id 	= $data[3];
			$item->route 			= $data[4];
			$item->old_route 		= $data[5];
			$item->port_date 		= $data[6];
			if (!empty($data[7])) {	$item->row_count = $data[7]; }
			$item->inserted_at 		= date("Y-m-d H:i:S");
			$item->operation_type 	= $operation_type;
			$item->hash 			= $line_hash;
			$item->save();

	        $row++;
	    }

		fclose($handle);

		Log::info('RI > End parsing Return Increment CSV ' . date("Y-m-d H:i:s"));
		// Log::info('------------------------------------------------------------');
	}

}
