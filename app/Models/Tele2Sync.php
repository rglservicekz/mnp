<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Log;

class Tele2Sync {

	public static function connect($operator)
	{
		// load MNP config
		$config = Config::get('mnp');

		// Connect and login
		$conn_id = ftp_connect($operator->host); 
		$login_result = ftp_login($conn_id, $operator->login, $operator->password); 

		if (!$conn_id)
		{
			$result = false;
		} else {
			$result = $conn_id;
		}

		return $result;
	}

	public static function download_port_all_full($sftp)
	{
		// load MNP config
		$config = Config::get('mnp');

		$af_files = scandir('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/');
		$af_filename = end($af_files);
		copy('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/' . $af_filename, $config['local_folder_kcell'] . $af_filename);

		// unzip file
		system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $af_filename);
		// copy recent fil to 'Recent' folder
		copy($config['local_folder_kcell'] . substr($af_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($af_filename, 0, -3) . 'csv');
		// remove archive, leave just csv file
		unlink($config['local_folder_kcell'] . $af_filename);

		return "ok";
	}

	public static function download_port_increment($ftp)
	{
		// load MNP config
		$config = Config::get('mnp');

		// Change dir to Tele2
		ftp_chdir($ftp, "tele2");
		$pi_files = ftp_nlist($ftp, "*");
		$pi_filename = end($pi_files);
		print_r($pi_files);
		die; 

		if(stristr($pi_filename, 'csv') === FALSE) {
			array_pop($pi_files);
		    $pi_filename = end($pi_files);
		}
		print_r($pi_filename);
		die; 

		copy('ssh2.sftp://' . $sftp . '/arch/Port_Increment/' . $pi_filename, $config['local_folder_kcell'] . $pi_filename);

		// unzip file  
		system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $pi_filename);
		// copy recent fil to 'Recent' folder
		copy($config['local_folder_kcell'] . substr($pi_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($pi_filename, 0, -3) . 'csv');
		// remove archive, leave just csv file
		unlink($config['local_folder_kcell'] . $pi_filename);

		return "ok";
	}

	public static function download_return_increment($sftp)
	{
		// load MNP config
		$config = Config::get('mnp');

		$ri_files = scandir('ssh2.sftp://' . $sftp . '/arch/Return_Increment/');
		$ri_filename = end($ri_files);
		copy('ssh2.sftp://' . $sftp . '/arch/Return_Increment/' . $ri_filename, $config['local_folder_kcell'] . $ri_filename);

		// unzip file
		system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $ri_filename);
		// copy recent fil to 'Recent' folder
		copy($config['local_folder_kcell'] . substr($ri_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($ri_filename, 0, -3) . 'csv');
		// remove archive, leave just csv file
		unlink($config['local_folder_kcell'] . $ri_filename);

		return "ok";
	}

}
