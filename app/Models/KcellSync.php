<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Log;

class KcellSync {

	public static function connect($operator)
	{
		// load MNP config
		$config = Config::get('mnp');

		$conn = ssh2_connect($operator->host, $operator->port);
		ssh2_auth_password($conn, $operator->login, $operator->password);
		$sftp = ssh2_sftp($conn);

		if (!$sftp)
		{
			$result = false;
		} else {
			$result = $sftp;
		}

		return $result;
	}

	public static function download_port_all_full($sftp)
	{
		// load MNP config
		$config = Config::get('mnp');
		$af_filename = 'notfound'; 

		$af_files = scandir('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/');

		$sd_time = strtotime(date("Y-m-d H:i:s"));
		$sd_time_mh = $sd_time - ((60 * 60) * 24);
		$sd_md = date("md", $sd_time_mh);

		$search_str = 'Port_All_Full_' . date("Y") . $sd_md . '0000'; 

		foreach($af_files as $k => $v) {
		    if(preg_match('/^' . $search_str . '/', $v)) {
		        $af_filename = $v;
		    } 
		}
		//echo $af_filename;  
		//die;  

		if (preg_match('/^Port/', $af_filename))
		{

			copy('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/' . $af_filename, $config['local_folder_kcell'] . $af_filename);

			// unzip file
			system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $af_filename);
			// copy recent fil to 'Recent' folder
			copy($config['local_folder_kcell'] . substr($af_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($af_filename, 0, -3) . 'csv'); 
			// remove archive, leave just csv file
			unlink($config['local_folder_kcell'] . $af_filename);

			return "ok";
		}

		return "error"; 
	}

	public static function download_port_increment($sftp)
	{
		// load MNP config
		$config = Config::get('mnp');
		$pi_filename = 'notfound'; 

		$pi_files = scandir('ssh2.sftp://' . $sftp . '/arch/Port_Increment/'); 
		//print_r($pi_files);    
		//die;   

		if (date("i") == 15) {
			$search_str = 'Port_Increment_' . date("YmdH") . '00';
		} else {
			$search_str = 'Port_Increment_' . date("YmdH") . '30'; 
		}
		
		foreach($pi_files as $k => $v) {
		    if(preg_match('/^' . $search_str . '/', $v)) {
		        $pi_filename = $v;
		    }
		}   

		if (preg_match('/^Port/', $pi_filename))
		{
			copy('ssh2.sftp://' . $sftp . '/arch/Port_Increment/' . $pi_filename, $config['local_folder_kcell'] . $pi_filename);

			// unzip file
			system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $pi_filename);
			// copy recent file to 'Recent' folder
			copy($config['local_folder_kcell'] . substr($pi_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($pi_filename, 0, -3) . 'csv');
			// remove archive, leave just csv file
			unlink($config['local_folder_kcell'] . $pi_filename);

			return "ok"; 
		}

		return "error"; 

	}

	public static function download_return_increment($sftp)
	{
		// load MNP config
		$config = Config::get('mnp');
		$ri_filename = 'notfound'; 

		$ri_files = scandir('ssh2.sftp://' . $sftp . '/arch/Return_Increment/'); 
		//$ri_filename = end($ri_files);
		//print_r($ri_files);
		//die; 

		if (date("i") == 15) {
			$search_str = 'Return_Increment_' . date("YmdH") . '00';
		} else {
			$search_str = 'Return_Increment_' . date("YmdH") . '30'; 
		}
		
		foreach($ri_files as $k => $v) {
		    if(preg_match('/^' . $search_str . '/', $v)) {
		        $ri_filename = $v;
		    } 
		}

		if ($ri_filename != 'notfound' && $ri_filename != '' && $ri_filename != 0)
		{

			copy('ssh2.sftp://' . $sftp . '/arch/Return_Increment/' . $ri_filename, $config['local_folder_kcell'] . $ri_filename);

			// unzip file
			system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $ri_filename);
			// copy recent fil to 'Recent' folder
			copy($config['local_folder_kcell'] . substr($ri_filename, 0, -3) . 'csv', $config['local_folder_recent'] . substr($ri_filename, 0, -3) . 'csv');
			// remove archive, leave just csv file
			unlink($config['local_folder_kcell'] . $ri_filename);

			return "ok";
		}

		return "error";		

	}


	////////////////////////////////////////////////
	public static function re_download_port_all_full($sftp, $filename)
	{
		// load MNP config
		$config = Config::get('mnp');
		$af_filename = 'notfound'; 

		$af_files = scandir('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/');

		foreach($af_files as $k => $v) {
		    if(preg_match('/^' . $filename . '/', $v)) {
		        $af_filename = $v;
		    } 
		}
		//echo $af_filename;  
		//die;  

		if (preg_match('/^Port/', $af_filename))
		{

			copy('ssh2.sftp://' . $sftp . '/arch/Port_All_Full/' . $af_filename, $config['local_folder_kcell'] . $af_filename);

			// unzip file
			system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $af_filename);
			// copy recent file to 'Recent late' folder
			copy($config['local_folder_kcell'] . substr($af_filename, 0, -3) . 'csv', $config['local_folder_recent_late'] . substr($af_filename, 0, -3) . 'csv'); 
			// remove archive, leave just csv file
			unlink($config['local_folder_kcell'] . $af_filename);

			return "ok";
		}

		return "error";

	}

	public static function re_download_port_increment($sftp, $filename)
	{
		// load MNP config
		$config = Config::get('mnp');
		$pi_filename = 'notfound'; 

		$pi_files = scandir('ssh2.sftp://' . $sftp . '/arch/Port_Increment/'); 
		//print_r($pi_files);    
		//die;   
		
		foreach($pi_files as $k => $v) {
		    if(preg_match('/^' . $filename . '/', $v)) {
		        $pi_filename = $v;
		    }
		}   

		if (preg_match('/^Port/', $pi_filename))
		{
			copy('ssh2.sftp://' . $sftp . '/arch/Port_Increment/' . $pi_filename, $config['local_folder_kcell'] . $pi_filename);

			// unzip file
			system('cd ' . $config['local_folder_kcell'] . ' && unzip -o ' . $config['local_folder_kcell'] . $pi_filename);
			// copy recent file to 'Recent' folder
			copy($config['local_folder_kcell'] . substr($pi_filename, 0, -3) . 'csv', $config['local_folder_recent_late'] . substr($pi_filename, 0, -3) . 'csv');
			// remove archive, leave just csv file
			unlink($config['local_folder_kcell'] . $pi_filename);

			return "ok"; 
		}

		return "error"; 

	}

}
