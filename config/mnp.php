<?php
// MNP config file
// 28/06/2015 @ Aibek Tanikenov

return [

	'local_folder_kcell' 			=> '/var/www/siesta2/mnp/storage/mnp_archives/kcell/', 

	'local_folder_tele2' 			=> '/var/www/siesta2/mnp/storage/mnp_archives/kcell/',

	'local_folder_beeline' 			=> '/var/www/siesta2/mnp/storage/mnp_archives/beeline/',

	'local_folder_altel' 			=> '/var/www/siesta2/mnp/storage/mnp_archives/altel/',

	'local_folder_recent' 			=> '/var/www/siesta2/mnp/storage/mnp_archives/recent/', 

	'local_folder_recent_late' 		=> '/var/www/siesta2/mnp/storage/mnp_archives/recent_late/', 
	
	
];